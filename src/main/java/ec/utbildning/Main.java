package ec.utbildning;

import java.util.List;
import java.util.Scanner;

public class Main {


    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws InvalidAnswerException, InterruptedException {
        slowLoading();
        menuOptions();
    }

    public static void slowLoading() throws InterruptedException {
        System.out.println("Booting up Calculator 3000 ...");
        System.out.print("Loading...");
        Thread.sleep(1000);
        System.out.print("Loading...");
        Thread.sleep(1000);
        System.out.println("Loading...");
        Thread.sleep(1000);
        System.out.println("Please wait...");
        Thread.sleep(1000);
        System.out.println("Finished booting up Calculator 3000");
        Thread.sleep(500);
    }

    public static void menuOptions() throws InvalidAnswerException {
        boolean inuse = true;
        while (inuse) {
            int choice = selectInput();
            switch (choice) {
                case 1:
                    handleAddition();
                    break;
                case 2:
                    handleSubtraction();
                    break;
                case 3:
                    handleMultiplikation();
                    break;
                case 4:
                    handleDivision();
                    break;
                case 5:
                    handleSquareRoot();
                    break;
                case 6:
                    inuse = false;
                    System.out.println("You've selected to quit the calculator. Goodbye!");
                    break;
                default:
                    System.out.println("Bad input");
            }
        }
    }

    public static int selectInput() throws InvalidAnswerException {


        List<Integer> acceptableAnswers = List.of(1, 2, 3, 4, 5, 6);
        System.out.println("Type the corresponding number to select your desired operation: ");
        System.out.println("1. Addition");
        System.out.println("2. Subtraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");
        System.out.println("5. Square root");
        System.out.println("6. Exit program");
        System.out.println("Your choice: ");
        int choice = scanner.nextInt();
        if (!acceptableAnswers.contains(choice)) {
            throw new InvalidAnswerException("Not a valid answer");
        } else {
            return choice;
        }
    }

    public static void handleAddition() {
        System.out.println("You've opted for addition");
        System.out.println("Enter your term: ");
        double term = scanner.nextDouble();
        System.out.println("Enter your second term ");
        double term1 = scanner.nextDouble();
        double sum = Operations.additive(term, term1);
        System.out.println("The sum of " + term + " added " + term1 + " is " + sum);


    }

    public static void handleSubtraction() {
        System.out.println("You've choose subtraction");
        System.out.println("Enter your first number: ");
        double term1 = scanner.nextDouble();
        System.out.println("Enter your second number: ");
        double term2 = scanner.nextDouble();
        double sum = Operations.subtract(term1, term2);
        System.out.println("The result of " + term1 + " subtracted of " + term2 + " is " + sum);
    }

    public static void handleMultiplikation() {
        System.out.println("You've opted for multiplication");
        System.out.println("Enter your first factor: ");
        double factor1 = scanner.nextDouble();
        System.out.println("Enter your second factor: ");
        double factor2 = scanner.nextDouble();
        double result = Operations.multiplication(factor1, factor2);
        System.out.println("The result of " + factor1 + " times by " + factor2 + " is " + result);

    }

    public static void handleDivision() {
        System.out.println("You've opted for division");
        System.out.println("Enter your numerator: ");
        double numerator = scanner.nextDouble();
        System.out.println("Enter your denominator: ");
        double denominator = scanner.nextDouble();
        double quotient = Operations.divide(numerator, denominator);
        System.out.println("The result of " + numerator + " divided by " + denominator + " is " + quotient);
    }

    public static void handleSquareRoot() {
        System.out.println("You've opted for square root");
        System.out.println("Enter your Square root number");
        double sqrtNumber = scanner.nextDouble();
        double proxValue = Operations.sqrt(sqrtNumber);
        System.out.println("The Square root of your number " + sqrtNumber + " is: " + proxValue);
    }


}
