package ec.utbildning;

public class Operations {


    public static double divide(double numerator, double denominator) {
        double result = numerator / denominator;
        return result;
    }

    public static double sqrt(double number) {
        double result = Math.sqrt(number);
        return result;
    }

    public static double additive(double term, double term1) {
        double result = term + term1;
        return result;
    }

    public static double multiplication(double factor1, double factor2) {
        double result = factor1 * factor2;
        return result;
    }


    public static double subtract(double term, double term2) {
        return term - term2;
    }
}


